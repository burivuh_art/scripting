import sys
from scripting.fs.storage.base import FileStorage


def search(data_dir, service_name, namespace, key, check_only=True):
    fs = FileStorage(data_dir, service_name)

    if check_only:
        res = [d for k, d in fs.iter_namespace(namespace, ['geo']) if k == key]
    else:
        res = [d for k, d in fs.iter_namespace(namespace) if k == key]

    return len(res), res[0]


if __name__ == '__main__':
    data_dir, service_name, namespace, key, check_only = sys.argv[1:]
    key = key.decode('utf8')
    check_only = True if check_only == 'True' else False
    total, first = search(data_dir, service_name, namespace, key, check_only)
    print 'Found:', total
    for name, value in first.iteritems():
        print '===', name, '==='
        if type(value) in (str, unicode):
            print value
        else:
            print u'/'.join(value)
