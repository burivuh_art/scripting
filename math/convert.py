import string

DIGITS = string.digits + string.lowercase


def uint2base(x, base, constant_length=None):
    '''Convert x from base=10 to base not bigger than a len(DIGITS)'''

    digits = []
    while x:
        digits.append(DIGITS[x % base])
        x /= base
    digits = list(reversed(digits))
    if constant_length:
        digits = ['0'] * (constant_length - len(digits)) + digits

    return ''.join(digits)
