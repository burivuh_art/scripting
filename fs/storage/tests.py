# -*- coding: utf-8 -*-

import unittest
import os
import time
import shutil

from scripting.fs.storage.base import FileProcessor, CompoundKeyFileProcessor
from scripting.fs.storage.base import FileStorage


class ProcessorTest(unittest.TestCase):
    def setUp(self):
        self._init_processor()
        self._init_data()
        self._prepare()

    def _init_processor(self):
        self.tested = FileProcessor()

    def _prepare(self):
        self.fname = '/tmp/promenad.contrib.filestorage.tests'
        try:
            os.remove(self.fname)
        except OSError:
            pass

        for key, data in self.records:
            self.tested.dump(self.fname, key, data)

        self.start_time = time.time()

    def tearDown(self):
        t = time.time() - self.start_time
        print "%s: %.3f sec" % (self.id(), t)


class TestDataProcessing(ProcessorTest):
    def _init_data(self):
        self.obj = (
            u'1',
            (u'русское имя', u'описание\nмногострочное\n\rи еще\tстрока\r')
        )
        self.records = tuple(
            (unicode(int(self.obj[0]) + i), self.obj[1])
            for i in range(3)
        )

    def test_escaping(self):
        records = tuple(self.tested.safe_load(self.fname))
        self.assertTrue(self.records == records)


class TestCompoundKeyDataProcessing(TestDataProcessing):
    def _init_processor(self):
        self.tested = CompoundKeyFileProcessor()

    def _init_data(self):
        self.obj = (
            (u'1', u'2'),
            (u'русское имя', u'описание\nмногострочное\n\rи еще\tстрока\r')
        )
        self.records = tuple(
            (
                (unicode(int(self.obj[0][0]) + i), self.obj[0][1]),
                self.obj[1]
            )
            for i in range(3)
        )


class TestProcessorLoad(ProcessorTest):
    convert = None
    unicode = False

    def _init_data(self):
        self.obj = (u'1', (u'русское имя', u'описание обычное'))
        self.records = [
            (
                unicode(int(self.obj[0]) + i),
                self.obj[1]
            )
            for i in range(10000)
        ]

    def test_fast_load(self):
        if self.convert:
            records = self.tested.fast_load(
                self.fname, convert=self.convert, unicode=self.unicode
            )
        else:
            records = self.tested.fast_load(self.fname, unicode=self.unicode)
        self.assertTrue(tuple(self.records) == tuple(records))

    def test_safe_load(self):
        if self.convert:
            records = self.tested.safe_load(
                self.fname, convert=self.convert, unicode=self.unicode
            )
        else:
            records = self.tested.safe_load(self.fname, unicode=self.unicode)
        self.assertTrue(tuple(self.records) == tuple(records))

    def test_search_and_load(self):
        key = unicode(int(self.obj[0]) + 10)
        if self.convert:
            record = self.tested.search_and_load(
                self.fname, key, convert=self.convert, unicode=self.unicode
            )
        else:
            record = self.tested.search_and_load(
                self.fname, key, unicode=self.unicode
            )
        self.assertTrue(record == (key, self.obj[1]))


class TestUnicodeProcessorLoad(TestProcessorLoad):
    unicode = True


class TestProcessorLoadFloat(TestProcessorLoad):
    convert = float

    def _init_data(self):
        self.obj = (u'1', (1, -5, 100))
        self.records = [
            (
                unicode(int(self.obj[0]) + i),
                self.obj[1]
            )
            for i in range(10000)
        ]


class TestUnicodeProcessorLoadFloat(TestProcessorLoadFloat):
    unicode = True


class TestCompoundKeyProcessorLoad(TestProcessorLoad):
    def _init_processor(self):
        self.tested = CompoundKeyFileProcessor()

    def _init_data(self):
        self.obj = (
            (u'1', u'2'), (u'русское имя', u'описание обычное')
        )
        self.records = [
            (
                (unicode(int(self.obj[0][0]) + i), self.obj[0][1]),
                self.obj[1]
            )
            for i in range(1000)
        ]

    def test_search_and_load(self):
        key = (unicode(int(self.obj[0][0]) + 100), self.obj[0][1])
        record = self.tested.search_and_load(self.fname, key)
        self.assertTrue(record == (key, self.obj[1]))


class TestStorageLoad(unittest.TestCase):
    data_convert = None
    unicode = False

    def _init_data(self):
        self.obj = (
            u'key123', {
                'text': (u'русское имя', u'описание\nмногострочное'),
                'geo': (u'12.54', u'13.78')
            }
        )

    def _init_storage(self):
        self.tested = FileStorage(self.fname, 'testservice')

    def setUp(self):
        self.fname = '/tmp/promenad.contrib.filestorage.root_dir'
        self.namespace = 'objects'

        self._init_data()

        shutil.rmtree(self.fname, ignore_errors=True)
        os.mkdir(self.fname)

        self._init_storage()

        self.tested.bulk_add(
            (self.namespace, self.obj[0], self.obj[1])
            for i in range(100000, 0, -1)
        )

        self.start_time = time.time()

    def tearDown(self):
        t = time.time() - self.start_time
        print "%s: %.3f sec" % (self.id(), t)

    def test_safe_load(self):
        if self.data_convert:
            objects = self.tested.load_namespace(
                self.namespace, data_convert=self.data_convert,
                unicode=self.unicode
            )
        else:
            objects = self.tested.load_namespace(
                 self.namespace, unicode=self.unicode
            )

        self.assertTrue(len(objects) == 1 and
                        objects[self.obj[0]]['text'] == self.obj[1]['text'] and
                        objects[self.obj[0]]['geo'] == self.obj[1]['geo'])

    def test_unsafe_load(self):
        if self.data_convert:
            objects = self.tested.load_namespace(
                self.namespace, data_convert=self.data_convert,
                unsafe=True, unicode=self.unicode
            )
        else:
            objects = self.tested.load_namespace(
                self.namespace, unsafe=True, unicode=self.unicode
            )

        self.assertTrue(len(objects) == 1 and
                        objects[self.obj[0]]['text'] == self.obj[1]['text'] and
                        objects[self.obj[0]]['geo'] == self.obj[1]['geo'])

    def test_iter_load(self):
        if self.data_convert:
            objects = dict(list(self.tested.iter_namespace(
                self.namespace, data_convert=self.data_convert,
                unicode=self.unicode
            )))
        else:
            objects = dict(list(self.tested.iter_namespace(
                self.namespace, unicode=self.unicode
            )))

        self.assertTrue(len(objects) == 1 and
                        objects[self.obj[0]]['text'] == self.obj[1]['text'] and
                        objects[self.obj[0]]['geo'] == self.obj[1]['geo'])


class TestUnicodeStorageLoad(TestStorageLoad):
    unicode = True


class TestStorageLoadFloat(TestStorageLoad):
    data_convert = float

    def _init_data(self):
        self.obj = (
            u'key123', {
                'text': (1.1, 0.5),
                'geo': (12.54, 13.78)
            }
        )


class TestUnicodeStorageLoadFloat(TestStorageLoadFloat):
    unicode = True


class TestStorageLoadCompoundKeyOn(TestStorageLoad):
    def _init_data(self):
        self.obj = (
            (u'key1', u'key2'), {
                'text': (u'русское имя', u'описание\nмногострочное'),
                'geo': (u'12.54', u'13.78')
            }
        )

    def _init_storage(self):
        self.tested = FileStorage(
            self.fname, 'testservice', key_type='compound'
        )


if __name__ == '__main__':
    unittest.main()
