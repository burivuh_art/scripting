# -*- coding: utf8 -*-

import hashlib
from scripting.math.convert import uint2base


def gen_key(*args):
    args = [unicode(arg).encode('utf8') for arg in args]
    h = hashlib.new('ripemd160')
    h.update(''.join(args))
    return h.hexdigest()


def key2int(key):
    return int(key, 16)


def int2key(key_int):
    return uint2base(key_int, 16, 40)
