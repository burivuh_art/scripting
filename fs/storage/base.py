# -*- coding: utf-8 -*-

import codecs
import os
import shutil
import errno
import functools


class FileStorageError(Exception):
    pass


class CorruptLineError(FileStorageError):
    pass


class BadNamespace(FileStorageError):
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return "Could not find path: '%s'" % self.path


class FoundTemporaryFiles(FileStorageError):
    pass


def unescape_unicode_data(data):
    return data.decode('unicode_escape')


def unescape_string_data(data):
    return data.decode('string_escape')


def dummy_unescape(data):
    return data


def escape_data(data):
    try:
        return data.encode('unicode_escape')
    except AttributeError:
        return unicode(data)


def dummy_escape(data):
    return unicode(data)


class FileProcessor(object):
    """
    Class for dumping and loading records (key, flat_data_list)
    in MapReduce compatible file format.
    Processor is using hardcoded knowledge that u'\n' is always a line breaker
    """
    KEY_DATA_LIM = '\t'
    DATA_LIM = '\t'
    RECORD_FORMAT = u'%s' + KEY_DATA_LIM + u'%s\n'

    @classmethod
    def dump(cls, fname, key, data_list, mode='a', escape=escape_data):
        return cls.bulk_dump(
            fname=fname,
            records=[(key, data_list)],
            mode=mode,
            escape=escape
        )

    @classmethod
    def bulk_dump(cls, fname, records, mode='a', escape=escape_data):
        with codecs.open(fname, mode, 'utf8') as fout:
            fout.write(cls.records_to_text(records, escape))

        return True

    @classmethod
    def records_to_text(cls, records, escape=escape_data):
        return u''.join(
            cls.RECORD_FORMAT % (
                key,
                cls.DATA_LIM.join(
                    escape(d) for d in data
                )
            )
            for key, data in records
        )

    @classmethod
    def get_opener(cls, fname, mode, unicode=True):
        if unicode:
            return codecs.open(fname, mode, 'utf8')
        return open(fname, mode)


    @classmethod
    def safe_load(cls, fname, convert=unescape_unicode_data, unicode=True):
        """
        Safely process records file @fname with returning a generator
        """
        with cls.get_opener(fname, 'r', unicode) as fin:
            for line in fin:
                res = cls.loads(line, convert)
                if res is not None:
                    yield res

    @classmethod
    def fast_load(cls, fname, convert=unescape_unicode_data, unicode=True):
        """
        Fast and unsafely process file @file returning a list of records
        """
        with cls.get_opener(fname, 'r', unicode) as fin:
            return [cls.loads(line, convert) for line in fin.readlines()]

    @classmethod
    def search_and_load(cls, fname, key, first=True,
                        convert=unescape_unicode_data, unicode=True):
        if not first:
            raise NotImplementedError()

        with cls.get_opener(fname, 'r', unicode) as fin:
            for line in fin:
                _key = cls.load_key(line)
                if _key == key:
                    return cls.loads(line, convert=convert)
        return None

    @classmethod
    def load_key(cls, line):
        try:
            return line.split(cls.KEY_DATA_LIM, 1)[0]
        except:
            raise CorruptLineError(
                'Corrupt data in line:%s' % repr(line)
            )

    @classmethod
    def loads(cls, line, convert=unescape_unicode_data):
        line = line.rstrip('\n')
        try:
            key, data_list = line.split(cls.KEY_DATA_LIM, 1)
        except:
            raise
            if not line:
                return None
            raise CorruptLineError(
                'Corrupt data in line:%s' % repr(line)
            )
        return key, tuple(
            convert(d)
            for d in data_list.split(cls.DATA_LIM)
        )

    @classmethod
    def loads_float(cls, line):
        return cls.loads(line, float)

    @classmethod
    def loads_raw(cls, line):
        return cls.loads(line, dummy_unescape)

    @staticmethod
    def count_records(fname):
        with open(fname, 'r') as fin:
            return sum(1 for _ in fin)


class CompoundKeyFileProcessor(FileProcessor):
    """
    FileProcessor that incapsulate correct dumping/loading compound keys
    """

    KEY_LIM = u'__'

    @classmethod
    def serialize_key(cls, key):
        return cls.KEY_LIM.join(map(unicode, key))

    @classmethod
    def deserialize_key(cls, key):
        return tuple(key.split(cls.KEY_LIM))

    @classmethod
    def records_to_text(cls, records, escape=escape_data):
        return u''.join(
            cls.RECORD_FORMAT % (
                cls.serialize_key(key),
                cls.DATA_LIM.join(
                    escape(d) for d in data
                )
            )
            for key, data in records
        )

    @classmethod
    def load_key(cls, line):
        try:
            return cls.deserialize_key(
                line.split(cls.KEY_DATA_LIM, 1)[0]
            )
        except:
            raise CorruptLineError(
                'Corrupt data in line:%s' % repr(line)
            )

    @classmethod
    def loads(cls, line, convert=unescape_unicode_data):
        line = line.rstrip('\n')
        try:
            key, data_list = line.split(cls.KEY_DATA_LIM, 1)
        except:
            if not line:
                return None
            raise CorruptLineError(
                'Corrupt data in line:%s' % repr(line)
            )
        return cls.deserialize_key(key), tuple(
            convert(d)
            for d in data_list.split(cls.DATA_LIM)
        )


class FileStorage(object):
    KEY_TYPE_PROCESSORS = {
        'plain': FileProcessor(),
        'compound': CompoundKeyFileProcessor()
    }
    DEFAULT_KEY_TYPE = 'plain'

    def __init__(self,
                 root_dir, owner_name,
                 sparse=False, key_type=DEFAULT_KEY_TYPE):
        self.directory = root_dir
        self.namespaces = []
        self.service = os.path.join(*owner_name.split('.'))
        self.sparse = sparse

        try:
            os.mkdir(os.path.join(self.directory, self.service))
        except OSError:
            pass

        self.file_processor = None
        self.set_key_type(key_type)

    def set_key_type(self, key_type):
        try:
            self.file_processor = self.KEY_TYPE_PROCESSORS[key_type]
        except KeyError:
            raise FileStorageError(
                'Unknown key type, use one of: %s' % (
                    self.KEY_TYPE_PROCESSORS.keys()
                )
            )

    def map_namespace_to_fs(self, parts):
        path = self.get_ns_dir(parts)
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
        return path

    def namespace_exists(self, namespace):
        path = self.get_ns_dir(self.split_namespace(namespace))
        return os.path.exists(path)

    @staticmethod
    def split_namespace(namespace):
        return namespace.split('.')

    @staticmethod
    def cat_namespace(ns_parts):
        return u'.'.join(ns_parts)

    def no_sharding(self, ns_shard):
        return ns_shard == self.no_shard_value()

    @staticmethod
    def no_shard_value():
        return '__all__'

    def get_filename(self, ns_parts, data_type, chunk=None):
        data_type = '%s.chunk%d' % (data_type, chunk) if chunk is not None else data_type
        return os.path.join(
            self.directory, self.service,
            *(ns_parts + [data_type])
        )

    def get_namespace_dir(self, namespace):
        return self.get_ns_dir(self.split_namespace(namespace))

    def get_ns_dir(self, ns_parts):
        return os.path.join(self.directory, self.service, *ns_parts)

    def get_ns_root_dir(self, ns_root):
        return os.path.join(self.directory, self.service, ns_root)

    def get_datatype_fname(self, path, data_type):
        return os.path.join(path, data_type)

    def get_namespace_datatype_fname(self, namespace, data_type):
        path = self.get_namespace_dir(namespace)
        return self.get_datatype_fname(path, data_type)

    def drop_column(self, namespace, data_type, ignore_errors=True):
        try:
            os.remove(
                self.get_filename(
                    self.split_namespace(namespace), data_type
                )
            )
        except OSError:
            if not ignore_errors:
                raise

    def add_record(self, namespace, key, **data_tuples):
        ns_parts = self.split_namespace(namespace)
        if namespace not in self.namespaces:
            self.map_namespace_to_fs(ns_parts)
            self.namespaces.append(namespace)
        for data_type, data_tuple in data_tuples.items():
            fname = self.get_filename(ns_parts, data_type)
            self.file_processor.dump(fname, key, data_tuple)

    def bulk_add(self, records, no_escaping=False, chunk=None):
        sorter = {}
        for namespace, key, data_tuples in records:
            for data_type, data_value in data_tuples.iteritems():
                (
                    sorter
                    .setdefault(namespace, {})
                    .setdefault(data_type, [])
                    .append((key, data_value))
                )
        for namespace, sorted_data in sorter.iteritems():
            ns_parts = self.split_namespace(namespace)
            if namespace not in self.namespaces:
                self.map_namespace_to_fs(ns_parts)
                self.namespaces.append(namespace)

            for data_type, data in sorted_data.iteritems():
                fname = self.get_filename(ns_parts, data_type, chunk)
                if no_escaping:
                    self.file_processor.bulk_dump(
                        fname, data, escape=dummy_escape
                    )
                else:
                    self.file_processor.bulk_dump(fname, data)

    def cat_chunks(self, namespace):
        for shard_path in self.iter_leaves(namespace):
            dtypes = {}
            for fname in os.listdir(shard_path):
                try:
                    dtype, chunk = fname.rsplit('.', 1)
                except ValueError:
                    continue

                if chunk.startswith('chunk'):
                    dtypes.setdefault(dtype, []).append(fname)
            for dtype in dtypes:
                dtypes[dtype] = sorted(dtypes[dtype])

            for dtype, sorted_fnames in dtypes.iteritems():
                with open(self.get_datatype_fname(shard_path, dtype), 'w') as accum_file:
                    for fname in sorted_fnames:
                        fname = self.get_datatype_fname(shard_path, fname)
                        with open(fname, 'r') as part_file:
                            accum_file.write(part_file.read())
                        os.unlink(fname)

    def drop_namespace(self, namespace, silent):
        ns_parts = self.split_namespace(namespace)
        shutil.rmtree(self.get_ns_dir(ns_parts), ignore_errors=silent)

    def recreate_namespace(self, namespace):
        ns_parts = self.split_namespace(namespace)
        path = self.get_ns_dir(ns_parts)
        shutil.rmtree(path, ignore_errors=True)
        return self.map_namespace_to_fs(ns_parts)

    def rename_namespace(self, from_namespace, to_namespace):
        from_parts = self.split_namespace(from_namespace)
        to_parts = self.split_namespace(to_namespace)

        shutil.move(self.get_ns_dir(from_parts), self.get_ns_dir(to_parts))

    def replace_namespace(self, to_replace, to_rename):
        self.drop_namespace(to_replace, silent=False)
        self.rename_namespace(to_rename, to_replace)

    def iter_leaves(self, namespace):
        ns_parts = self.split_namespace(namespace)

        path = self.get_ns_dir(ns_parts)
        if not os.path.exists(path) or not os.path.isdir(path):
            raise BadNamespace(path)

        for root, dirs, files in os.walk(path, followlinks=True):
            if not dirs:
                yield root

    def get_update_times(self, namespace, dtypes=tuple()):
        timestamps = []
        for shard_path in self.iter_leaves(namespace):
            dtypes = dtypes or os.listdir(shard_path)
            for dtype in dtypes:
                fname = self.get_datatype_fname(shard_path, dtype)
                timestamps.append(os.path.getmtime(fname))

        return min(timestamps), max(timestamps)

    def list_sublevel(self, root_namespace):
        return os.listdir(self.get_ns_root_dir(root_namespace))

    def list_subnamespaces(self, namespace):
        if not isinstance(namespace, unicode):
            raise ValueError('Namespace argument MUST be Unicode')
        return [
            self.get_namespace_by_path(p)
            for p in self.iter_leaves(namespace)
        ]

    def get_namespace_by_path(self, shard_path):
        # TODO: linux-only
        relpath = os.path.relpath(
            shard_path,
            start=os.path.join(self.directory, self.service)
        )
        return relpath.replace('/', '.')

    def load_namespace(self, namespace,
                       load_data_types=None, data_convert=None,
                       unsafe=False, unicode=True):
        ns_shards = self.iter_leaves(namespace)

        loader = self.file_processor.safe_load
        if unsafe:
            loader = self.file_processor.fast_load
        if data_convert:
            loader = functools.partial(loader, convert=data_convert)

        result_objects = {}
        for shard_path in ns_shards:
            shard_namespace = self.get_namespace_by_path(shard_path)
            dtypes = os.listdir(shard_path)
            if load_data_types:
                dtypes = [dt for dt in dtypes if dt in load_data_types]

            for data_type in dtypes:
                if not load_data_types or data_type in load_data_types:
                    records = loader(
                        self.get_datatype_fname(shard_path, data_type),
                        unicode=unicode
                    )

                    for key, data_tuple in records:
                        o = result_objects.setdefault(
                            key,
                            {'namespace': shard_namespace, 'key': key}
                        )
                        o[data_type] = data_tuple

        return result_objects

    def iter_namespace(self, namespace,
                       load_data_types=None, data_convert=None,
                       counter=None, unicode=True):
        ns_shards = self.iter_leaves(namespace)

        count_records = 0

        for shard_path in ns_shards:
            shard_namespace = self.get_namespace_by_path(shard_path)
            dtypes = os.listdir(shard_path)
            if load_data_types:
                dtypes = [
                    dt for dt in dtypes if dt in load_data_types
                ]

            dtypes_fname = {
                dtype: self.get_datatype_fname(shard_path, dtype)
                for dtype in dtypes
            }
            if self.sparse:
                dtypes_len = {
                    dtype: self.file_processor.count_records(
                        dtypes_fname[dtype]
                    )
                    for dtype in dtypes
                }
                dtypes.sort(
                    key=lambda e: dtypes_len[dtype],
                    reverse=True
                )

            def get_processor(data_convert):
                if data_convert:
                    return self.file_processor.safe_load(
                        dtypes_fname[dtype], convert=data_convert,
                        unicode=unicode
                    )
                return self.file_processor.safe_load(dtypes_fname[dtype])

            processors = [
                (
                    dtype,
                    get_processor(data_convert),
                    {}
                )
                for dtype in dtypes
            ]

            while True:
                result_object = {'namespace': shard_namespace}

                for dtype, proc, cache in processors:
                    if 'key' not in result_object:
                        if cache:
                            item = next(cache.itervalues(), None)
                            if item is not None:
                                del cache[item[0]]
                            else:
                                item = next(proc, None)
                        else:
                            item = next(proc, None)

                        if item is None:
                            continue

                        result_object['key'] = item[0]
                        result_object[dtype] = item[1]
                    else:
                        key_to_find = result_object['key']
                        add_value = cache.get(key_to_find, None)
                        if add_value:
                            del cache[key_to_find]

                        while not add_value:
                            item = next(proc, None)
                            if item is None:
                                break

                            if item[0] == key_to_find:
                                add_value = item
                            else:
                                cache[item[0]] = item

                        if add_value:
                            result_object[dtype] = add_value[-1]

                if 'key' not in result_object:
                    break

                if counter:
                    count_records += 1
                    counter(count_records)

                yield result_object['key'], result_object
